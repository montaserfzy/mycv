export const Reviewer = [
    {
        name: 'Tamer Qtaish',
        position: 'CTO at homie.rent',
        note: 'Muntasir does not say much! Even when midnight strikes with everyone scrambling to deliver things in time, Muntasir quietly builds a world of front-end magic! Muntasir embodies "It\'s not about what you say, its about what you do." Technically Muntasir is a rare talent with many skills and always a solid team member to relay on for advice and help',
        image: 'https://media.licdn.com/dms/image/C5603AQHTf1ovYLdFpA/profile-displayphoto-shrink_100_100/0?e=1568851200&v=beta&t=yOZjhMxt8AxPnX0RTFaSWNS-z2cE7BMUTWvT9K1rKrQ'
    },
    {
        name: 'Ahmad Al Tamimi',
        position: 'Senior Software Engineer at CASHU',
        note: 'I worked with Montaser for more than two years, he is one of the most creative, hard working professionals I have worked with during my career. His passion to software development and his self-learning skills are a key drive in the success of any product he works on. I believe Montaser will enrich any team he joins.',
        image: 'https://media.licdn.com/dms/image/C5603AQHEHw8qeY4fkQ/profile-displayphoto-shrink_100_100/0?e=1568851200&v=beta&t=BeBZ45xVpnASKnBvQJBtoC7oIGqnqNKpx_VaYwj9stE'
    },
    {
        name: 'Fateen AL Zu\'bi',
        position: 'Full Stack Senior Wep Application Developer',
        note: 'Montaser is a talented Web Developer and great to work with. Not only is he an amazing designer, but he has ingenious web skills with an eye for detail. Montaser is very innovative and always looking for new solutions and fresh ideas that can advance his knowledge to exceed his client’s expectations and goals. Montaser a fun person to work with, and is always helpful and insightful. He is a passionate designer and always seems to have great awareness of new creative strategies and technology, which makes him a great asset to any company.',
        image: 'https://media.licdn.com/dms/image/C4E03AQGv-38Q7uLKIQ/profile-displayphoto-shrink_100_100/0?e=1568851200&v=beta&t=IGQqRni91weg2IpNoljIyvM-jCj_wHoXwXQvSv76eXc'
    },
    {
        name: 'Hashem Shmaisani (MSc,CCNA,CWTS,CEH)',
        position: 'E-learning and Training Specialist  at Alef Education, Edu-Tech, Cybersecurity, IoT, Blockchain',
        note: 'I\'ve worked with montaser in hyper-execution as a web developer teammate he is a great PHP programmer, an excellent template editor and designer, a great team member, also he has great communication skills and helping skills, so i recommend him without reservation or hesitation to any related work and wishing him all success in his future work and development.',
        image: 'https://media.licdn.com/dms/image/C5103AQGAakHuq06tHQ/profile-displayphoto-shrink_100_100/0?e=1568851200&v=beta&t=6Q5VMFrP-8Q9tYGO4ZXSOVv0q9FoIQ571QHLOlaeYCc'
    }

];
export const Companies = [
    {
        lat: '31.9794801',
        lng: '35.8453979',
        name: 'MenaItech',
        link: 'http://www.menaitech.com'
    },
    {
        lat: '32.0183224',
        lng: '35.8340445',
        name: 'EtQ. Inc',
        link: 'http://www.etq.com'
    },
    {
        lat: '31.9714132',
        lng: '35.8344673',
        name: 'MBC Group',
        link: 'http://www.mbc.net'
    },
    {
        lat: '51.5212934',
        lng: '-0.2046893',
        name: 'Homie.rent',
        link: 'http://www.Homie.rent'
    }
];


export const Skills = [
    {
        name: 'Javascript',
        value: '95'
    },
    {
        name: 'HTML5',
        value: '90'
    },
    {
        name: 'CSS3',
        value: '90'
    },
    {
        name: 'SCSS',
        value: '90'
    },
    {
        name: 'ReactJs',
        value: '80'
    },
    {
        name: 'ReactNative',
        value: '85'
    },
    {
        name: 'AngularJs',
        value: '60'
    },
    {
        name: 'Ionic',
        value: '70'
    },
    {
        name: 'PhoneGap and Cordova',
        value: '60'
    },
    {
        name: 'NodeJS',
        value: '60'
    },
    {
        name: 'PHP - Codigniter',
        value: '70'
    },
    {
        name: 'MySql',
        value: '60'
    },
    {
        name: 'Ruby on Rails',
        value: '30'
    },
];

export const Education = [
    {
        date: '2008 - 2012',
        position: 'Bachelor of Education (BEd)',
        companyName: 'Princess Sumiya University For Technology',
        note: 'Attend on Injaz Courses,Yahoo developing day (Hack U) , Google day and Nokia day'
    }, {
        date: '2014',
        position: 'Microsoft - Credential ID 70-480',
        companyName: 'Programming in HTML5 with JavaScript and CSS3',
        note: 'Candidates for this exam are developers with at least one year of experience developing with HTML in an object-based, event-driven programming model, and programming essential business logic for a variety of application types, hardware, and software platforms using JavaScript.'
    },
];

export const Experience = [
    {
        date: 'Jan 2018 - Present',
        position: 'Senior Frontend Engineer',
        companyName: 'Homie.rent',
        note: 'Front-end development using latest HTML5 and CSS3 techniques, backed by jQuery and other Javascript frameworks to enable optimum user experience throughout the site. '
    }, {
        date: 'Jan 2016 – Jun 2018',
        position: 'Frontend Engineer',
        companyName: 'Middle East Broadcasting Center',
        note: 'Worked on mbc.net and mbc3.net sites to add enhancements and bug fixes, building mobile and web application form mbc3 on reactJs and react-native'
    },
    {
        date: 'Aug 2014 – Dec 2015',
        position: 'Front End Engineer',
        companyName: 'EtQ',
        note: 'Build tragapth mobile application and responsible on features, bugs, enhancements and technologies used in the project, collaborate with design and Backend teams to push app online'
    },
    {
        date: 'Jun 2012 – Aug 2014',
        position: 'PHP Software Engineer (Full Stack)',
        companyName: 'MenaITech',
        note: 'Responsible on switching Human Resource Management System from offline software to online and lite version'
    }
];


